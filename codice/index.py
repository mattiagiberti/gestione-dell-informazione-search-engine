from whoosh.analysis.analyzers import RegexTokenizer
from whoosh.analysis.filters import LowercaseFilter, StopFilter
from whoosh.analysis import Filter
from whoosh.analysis.morph import StemFilter
from whoosh.fields import Schema, TEXT, ID
from whoosh import index
from nltk import WordNetLemmatizer
from whoosh.analysis import CharsetFilter
from whoosh.support.charset import default_charset, charset_table_to_dict
from whoosh.analysis import StandardAnalyzer
import xml.etree.ElementTree as ET
import time

from whoosh.query.terms import Regex

dump_xml = "dump/dump_16kdoc.xml"

class Index():
    def __init__(self):
        #The schema specifies the fields of documents in an index
        #TEXT fields use StandardAnalyzer by default. To specify a different analyzer, use the analyzer keyword argument to the constructor
        #TEXT(stored=True) to specify that the text should be stored in the index
        #Use ID for fields like url or path (the URL or file path of a document), date, category – fields where the value must be treated as a whole, and each document only has one value for the field
        self.schema = Schema(id=ID(stored=True),
                             url=ID(stored=True),
                             title=TEXT(stored=True, analyzer=myAnalyzer(), phrase=True),
                             content=TEXT(stored=True, analyzer=myAnalyzer(), phrase=True, spelling=True))
        self.idx_path = "index_folder"
        self.idx = None
        self.writer = None
        self.reader = None

    def creaIndex(self):
        if not index.exists_in(self.idx_path):
            self.idx = index.create_in(self.idx_path, self.schema)
            if not self.idx:
                raise FileNotFoundError("errore indice")
            self.writer = self.idx.writer()
            
            #inizio indexing
            start = time.time()
            tree = ET.parse(dump_xml)
            root = tree.getroot()
            doc_processed = 0
            tot_doc = len(list(root))
            for x in range(tot_doc):
                self.writer.add_document(
                    id = root[x][0].text,
                    url = root[x][1].text,
                    title = root[x][2].text,
                    content = root[x][3].text
                )
                doc_processed +=1
                print("Indexing status: {:.0f}% ({}/{})".format(100*doc_processed/tot_doc, doc_processed, tot_doc))
            self.writer.commit()
            end = time.time() - start
            #fine indexing

            print("Tempo impiegato per la creazione dell'indice: {:.0f} secondi".format(end))
        else:
            print("Indice esistente in: ../{}".format(self.idx_path))
            self.idx = index.open_dir(self.idx_path)
        
        self.reader = self.idx.reader()


#PREPROCESSING
#change analyzer to make improvements
def myAnalyzer():

    
    #Il primo elemento deve essere un tokenizer e i restanti sono filtri
    #es: my_analyzer = RegexTokenizer() | LowercaseFilter() | StopFilter()

    #V2
    #my_analyzer = RegexTokenizer() | LowercaseFilter()

    #V3
    #my_analyzer = RegexTokenizer() | LowercaseFilter() | StopFilter()

    #V4
    #my_analyzer = RegexTokenizer() | LowercaseFilter() | StemFilter()

    #V5
    #my_analyzer = RegexTokenizer() | LowercaseFilter() | MyLemmatizer()

    #V6
    charmap = charset_table_to_dict(default_charset)
    my_analyzer = RegexTokenizer() | LowercaseFilter() | StemFilter() | CharsetFilter(charmap)

    return my_analyzer

class MyLemmatizer(Filter):
    def __call__(self, tokens):
        for token in tokens:
            token.text = WordNetLemmatizer().lemmatize(token.text)
            yield token

