from whoosh import scoring
from whoosh.qparser import MultifieldParser
from whoosh.scoring import TF_IDF, BM25F
from whoosh.searching import Searcher as Whoosh_Searcher


class Searcher:
    def __init__(self, index):
        self.index = index
        self.parser = MultifieldParser(["title", "content"], fieldboosts={'title': 2, 'content': 1}, schema=self.index.schema)

        self.searcher = {
            'bm25': Whoosh_Searcher(reader=self.index.reader, weighting=scoring.BM25F),
            'tf_idf': Whoosh_Searcher(reader=self.index.reader, weighting=scoring.TF_IDF),
            'frequency': Whoosh_Searcher(reader=self.index.reader, weighting=scoring.Frequency),
        }

    def my_search(self, text):

        my_query = self.parser.parse(text)
        my_res = []
     
        #BM25
        searcher = self.searcher['bm25']

        #tf_idf
        #searcher = self.searcher['tf_idf']

        #freq
        #searcher = self.searcher['frequency']

        results = searcher.search(my_query, limit = 10)

        for res in results:
            print(res.score)
            my_res.append(Risultato(id = res['id'], url = res['url'], titolo = res['title']))

        return my_res 


class Risultato():
    def __init__(self, id, url, titolo):
        self.id = id
        self.url = url
        self.titolo = titolo
