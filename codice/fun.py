import math

iDCG = 14.8277 

def nDCG(rel_list):
    dcg = 0

    for x in range(len(rel_list)):
        dcg += (rel_list[x])/round((math.log2(x+2)),2)

    #print("ndcg: ", round(dcg/iDCG,2))
    return round(dcg/iDCG, 3)
   
def averagePrecision(my_Se_list, test_set_list):
    hit = 0
    tot_precision = 0 
    for n in range(len(my_Se_list)):
        if my_Se_list[n] in test_set_list:
            hit += 1
            tot_precision += hit/(n+1)
    if hit == 0: return 0
    return round(tot_precision/hit, 3)