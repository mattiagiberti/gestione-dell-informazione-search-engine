import time
import webbrowser
import tkinter as tk
import site
import os
import enchant
import nltk
from tkinter.scrolledtext import ScrolledText
from codice.index import Index
from codice.search import Searcher
from codice.fun import nDCG, averagePrecision

nltk.download('punkt')

larghezza = 1920
altezza = 1000

google_results = {
  "DNA": ['7955', '376524', '9329150', '21496', '26846483', '12388', '4250553', '21505', '4292', '25758'],
  "Apple": ['856', '2593693', '1344', '501016', '16161443', '19006979', '8841749', '16179920', '7412236', '5078775'],
  "Epigenetics": ['49033', '37122597', '35812169', '31182307', '46697210', '14938064', '42647878', '56840878', '42684750', '30932051'],
  "Hollywood": ['53849', '84485', '990782', '1423694', '914512', '27421416', '871371', '105776', '2539634', '56717294'],
  "Maya": ['18449273', '77041', '355254', '113014', '4967305', '5958027', '147476', '9617641', '1494968', '1524237'],
  "Microsoft": ['19001', '1545193', '4240094', '18784745', '18890', '709683', '20288', '6947642', '4395735', '16358617'],
  "Precision": ['14343887', '41572', '1164930', '18421531', '24467258', '866638', '10614570', '11946231', '17449489', '3962145'],
  "Tuscany": ['21967242', '14448498', '679153', '9083550', '319100', '4535978', '59217770', '548647', '46169', '11525'],
  "99 balloons": ['884209', '6687843', '345742', '209669', '11124013', '1572117', '2964276', '32922685', '15290506','25349705'],
  "Computer Programming": ['5311', '5783', '23015', '144146', '6301802', '4119246', '6021', '27661', '3882218', '189897'],
  "Financial meltdown": ['2878852', '32005855', '21553168', '19337279', '22652683', '10062100', '735925', '26152387', '1693352', '933495'],
  "Justin Timberlake": ['69323', '10429878', '35974124', '37620375', '2300849', '7325720', '40245278', '56196328', '38453748', '466563'],
  "Least Squares": ['82359', '27118759', '1651906', '971437', '15652764', '4504135', '2593771', '2407860', '1046736', '2713327'],
  "Mars robots": ['421366', '252908', '36645032', '1632896', '426143', '10297736', '37837437', '421051', '421049', '21134515'],
  "Page six": ['2562498', '102227', '56208270', '483472', '207444', '23257580', '20931146', '21599513', '4690051', '2016668'],
  "Roman Empire": ['25507', '504379', '219117', '17958273', '25791', '3072770', '22290735', '923406', '521555', '314732'],
  "Solar energy": ['27743', '13690575', '3507365', '32793086', '6328047', '413092', '54269557', '652531', '17805223', '35295938'], 
  "Statistical Significance": ['160995', '554994', '30284', '1311951', '5657877', '935655', '1361141', '226680', '9444220', '23658675'],
  "Steve Jobs": ['7412236', '44519131', '33428137', '45145038', '47603179', '35304421', '37656556', '46240685', '864271', '27848'],
  "The Maya": ['18449273', '355254', '46998769', '4967305', '6721754', '30974186', '5958027', '10443117', '1494968', '21110575'],
  "Triple Cross": ['9283532', '10139650', '9283529', '978650', '48613699', '1687809', '40462433', '7987443', '10906715', '46436562'],
  "US Constitution": ['31644', '31646', '497752', '9119240', '31647', '932529', '31650', '31653', '31666', '31662'],
  "Eye of Horus": ['967176', '1350710', '49448', '24836888', '23409619', '506916', '97350', '69035', '49471', '59236'],
  "Madam I'm Adam": ['24147', '41686424', '44014576', '46519718', '299854', '890744', '5941577', '21166371', '2808119', '45435088', '10967900'],
  "Mean Average Precision": ['50716473', '14343887', '11184711', '15271', '3440396', '8379669', '41932', '43218024', '33274', '25050663'], 
  "Physics Nobel Prizes": ['52497', '1175987', '21201', '1528075', '9053562', '19653466', '1175987', '1811842', '43229605', '24636986'],
  "Read the manual": ['136530', '316050', '5655064', '40079206', '1256791', '44484413', '749736', '35495222', '85332', '1472613'],
  "Spanish Civil War": ['18842471', '9468339', '31596058', '31594699', '1555138', '24125782', '31842251', '32580812', '15369', '58859770'],
  "Do geese see god": ['44014576', '24147', '2471828', '8124818', '50484', '25075', '29208204', '25477162', '23191053', '746138'], 
  "Much ado about nothing": ['8781048', '1615355', '33528868', '41517346', '27817491', '57465117', '33568270', '2612489', '3113126', '31323944']
}

query_30_test = ["DNA", "Apple", "Epigenetics", "Hollywood", "Maya",
                  "Microsoft", "Precision", "Tuscany", "99 balloons", "Computer Programming",
                  "Financial meltdown", "Justin Timberlake", "Least Squares", "Mars robots", "Page six",
                  "Roman Empire", "Solar energy", "Statistical Significance", "Steve Jobs", "The Maya",
                  "Triple Cross", "US Constitution", "Eye of Horus", "Madam I'm Adam", "Mean Average Precision",
                  "Physics Nobel Prizes", "Read the manual", "Spanish Civil War", "Do geese see god",
                  "Much ado about nothing"]

GO_static_rel_dict = {
    1: 6,
    2: 5,
    3: 4,
    4: 3,
    5: 2,
    6: 1,
    7: 1,
    8: 1,
    9: 1,
    10: 1
}

# benchmark = true solo quando si vogliono lanciare tutte le query di prova insieme per valutare il sistema
def searching(searcher, input_query, benchmark, listbox, textbox):
    map = ndcg_media = map_not_divided = ndcg_not_divided = 0
    listbox.delete(0, "end")

    if benchmark:
        # Valutazione ricerca sulle 30 query
        for query in query_30_test:
            avg = ndcg = 0

            relevance_list = [] * 10
            my_Se_list_10 = []

            go_rel_dict = getGoogleId(query)

            #searching
            res = searcher.my_search(query)
            
            for r in res:
                my_Se_list_10.append(r.id)

            #average precision
            most_relevant_10 = google_results.get(query) #prende la lista dei 10 risultati più rilevanti per una query nel testset di google
            avg = averagePrecision(my_Se_list_10, most_relevant_10)

            #nDCG
            for my_result in my_Se_list_10:
                if my_result in go_rel_dict.keys():
                    relevance_list.append(go_rel_dict.get(my_result))
                else:
                    relevance_list.append(0)
            
            ndcg = nDCG(relevance_list)

            map_not_divided += avg
            ndcg_not_divided += ndcg
            
            listbox.insert("end", "Query: {: <30} avg PRECISION: {: <10} nDCG: {: <10}".format(query, avg, ndcg))

        ndcg_media = round(ndcg_not_divided/30,3)
        map = round(map_not_divided/30, 3) #30 come le query che compongono il testset
        
        label = "Valutazione finale             mAP: {:<10} nDCG media: {:<10}".format(map, ndcg_media)
        listbox.insert("end", label)

        print("ndcg 30 query: ", ndcg_media)
        print("mAP 30 query: ", map)
    else:
        # Ricerca da search tab
        textbox.delete("1.0", tk.END)
        query = input_query.get()

        start = time.time()
        results = searcher.my_search(query)
        time_lapsed = round(time.time() - start, 3) 
        if len(results) == 0:
            listbox.insert("end", "No results found")
        else:
            listbox.insert("end", "Time for searching: {} seconds".format(time_lapsed))

        if len(results) < 10:
            textbox.insert("end","Few results found, did you mean: {}".format(didYouMean(query)))

        for x in range(len(results)):
            item = results[x]
            listbox.insert("end", "Result {}".format(x+1))
            listbox.insert("end", item.url)
            listbox.insert("end", "Title: {}".format(item.titolo))
            listbox.insert("end", "")

def didYouMean(query):
    suggerite = []
    ench_dict = enchant.Dict("en_US")
    query = nltk.word_tokenize(query)

    for word in query:
        print(word)
        for w in ench_dict.suggest(word):
            if w not in word:
                suggerite.append(w)

    return suggerite

def getGoogleId(q):
    google_res_rel = [6,5,4,3,2,1,1,1,1,1]
    google_rel_dict = {}

    go_list = google_results[q]

    for x in range(10):
        google_rel_dict[go_list[x]] = google_res_rel[x]

    return google_rel_dict

def OpenUrl(event):
    widget = event.widget
    index = int(widget.curselection () [0])
    url = widget.get(index)
    if url.startswith("https://en.wikipedia.org"):
        webbrowser.open_new (url)

if __name__ == '__main__':
    site.addsitedir(os.path.dirname(os.path.realpath(__file__)) + '/codice')
    index = Index()
    index.creaIndex()
    searcher = Searcher(index)
    root = tk.Tk()
    background_color = "light blue"
    root.title("Search Engine")
    root.geometry("{}x{}".format(larghezza, altezza))
    root.resizable(None, None)
    canvas = tk.Canvas(root, width=larghezza, height=altezza)
    frame = tk.Frame(canvas, background=background_color)
    vsb = tk.Scrollbar(root, orient="vertical", command=canvas.yview)
    canvas.configure(yscrollcommand=vsb.set)
    vsb.pack(side="right", fill="y")
    canvas.pack(side="left", fill="both", expand=True)
    canvas.create_window((4, 4), width=larghezza, window=frame, anchor="nw")

    p0 = tk.PanedWindow(frame, width=larghezza, background=background_color)
    p1 = tk.PanedWindow(frame, background=background_color)
    p2 = tk.PanedWindow(frame, background=background_color)
    p3 = tk.PanedWindow(frame, background=background_color)
    p4 = tk.PanedWindow(frame, background=background_color)
    p5 = tk.PanedWindow(frame, background=background_color)

    p0.grid()
    p1.grid()
    p2.grid()
    p3.grid()
    p4.grid()
    p5.grid()

    padding = 20

    label0 = tk.Label(p1, text="Search Engine", background=background_color)
    label0.config(font=("Aerial", 24))
    p1.add(label0)

    input_query = tk.Entry(width=50)
    p2.add(input_query, pady=padding)

    search_btn = tk.Button(text="Search", relief="groove", padx=padding, command=lambda: searching(searcher, input_query, False, listbox_res, textbox))
    p2.add(search_btn, padx=padding, pady=padding)

    benchmark_btn = tk.Button(text="Benchmark with Google Test Set", relief="groove", padx=padding, command=lambda: searching(searcher, input_query, True, listbox_val, None))
    p3.add(benchmark_btn, padx=padding, pady=padding)

    results_lbl = tk.Label(p4, text="Results", background=background_color)
    results_lbl.config(font=("Aerial", 20))
    results_lbl.grid(row=0, column=0)

    listbox_res = tk.Listbox(p4, width=80, height=15) #risultati
    listbox_res.config(font=("Courier",10))
    listbox_res.grid(row=1, column=0, padx=10)
    listbox_res.bind("<<ListboxSelect>>", OpenUrl)

    eval_lbl = tk.Label(p4, text="Evaluation", background=background_color)
    eval_lbl.config(font=("Aerial", 20))
    eval_lbl.grid(row=0, column=1, padx=10)

    listbox_val = tk.Listbox(p4, width=80, height=15) #valutazione
    listbox_val.config(font=("Courier",10))
    listbox_val.grid(row=1, column=1, padx=10)

    dym_lbl = tk.Label(p4, background=background_color)
    dym_lbl.config(font=("Aerial", 10))
    dym_lbl.grid(row=2, column=0, padx=10)

    textbox = ScrolledText(p5, height = 10) #did you mean
    textbox.grid(row=0, column=0)

    input_query.focus_set()

    root.mainloop()
