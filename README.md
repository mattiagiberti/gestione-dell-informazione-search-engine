# Progetto
Motore di ricerca che utilizza Whoosh per recuperare i migliori risultati da un dump di Wikipedia costituito da circa 16'000 documenti, a seguito di una query immessa dall'utente.

# Requisiti
- python3.9
- pipenv
- vedi altri nella sezione "comandi per l'installazione"

# Comandi per l'installazione
```bash
pipenv shell
pip install Whoosh
pip install nltk
pip install pyenchant
```
# Esecuzione
```bash
python searchEngine.py
```
# Alla prima esecuzione viene creato un nuovo indice (la procedura potrebbe richiedere qualche minuto)
